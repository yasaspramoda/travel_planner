package test.pramoda.travelplanner.exception;

public class WeatherException extends Exception {

    public WeatherException(String message) {
        super(message);
    }

}
