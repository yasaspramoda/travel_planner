package test.pramoda.travelplanner.service.impl;

import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.spring.annotations.Recurring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.pramoda.travelplanner.model.Forecast;
import test.pramoda.travelplanner.repository.ForecastRepository;
import test.pramoda.travelplanner.service.CacheClearService;

import java.sql.Timestamp;
import java.util.List;

@Service
public class CacheClearServiceImpl implements CacheClearService {

    @Autowired
    private ForecastRepository forecastRepository;

    @Override
    @Transactional
    @Recurring(id = "cache-cleaning-job", cron = "*/15 * * * *") //Run every 15 minutes
    @Job(name = "Cache cleaning job")
    public void clearRecords() {
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        List<Forecast> oldRecords = forecastRepository.findByCreatedAtGreaterThanHour(currentTime);
        forecastRepository.deleteAll(oldRecords);
    }
}
