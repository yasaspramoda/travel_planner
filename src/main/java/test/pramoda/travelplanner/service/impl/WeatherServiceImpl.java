package test.pramoda.travelplanner.service.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import test.pramoda.travelplanner.exception.WeatherException;
import test.pramoda.travelplanner.model.Forecast;
import test.pramoda.travelplanner.repository.ForecastRepository;
import test.pramoda.travelplanner.service.WeatherService;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class WeatherServiceImpl implements WeatherService {

    //TODO: tests
    Logger logger = LoggerFactory.getLogger(WeatherServiceImpl.class);

    @Autowired
    private ForecastRepository forecastRepository;

    @Override
    public Map<String, Object> getWeatherForCity(String cityName) throws WeatherException {
        return callWeatherAPI(weatherURL, cityName);
    }

    @Override
    @Transactional
    public List<Forecast> getForecastForCity(String cityName, String dateTime) throws WeatherException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(dateTime);
        } catch (ParseException e) {
            logger.error("Unable to parse datetime string provided: " + dateTime);
            throw new WeatherException("Unable to parse datetime string provided: " + dateTime);
        }
        Timestamp timestamp = new Timestamp(parsedDate.getTime());

        List<Forecast> forecastList = forecastRepository.findByCityAndDateTime(cityName, timestamp);

        if (forecastList.isEmpty()) {
            Map<String, Object> results = callWeatherAPI(forecastURL, cityName);
            forecastList = processResults(results, timestamp);
            forecastRepository.saveAll(forecastList);
        } else {
            logger.info("Cached results returned from internal DB" + forecastList);
        }

        return forecastList;
    }

    private List<Forecast> processResults(Map<String, Object> results, Timestamp dateTime) {
        List<Forecast> forecastList = new ArrayList<>();

        Forecast forecast = new Forecast();
        forecast.setCity((String) ((Map<String, Object>) results.get("city")).get("name"));
        forecast.setCountryCode((String) ((Map<String, Object>) results.get("city")).get("country"));

        List<Object> forecasts = (List<Object>) results.get("list");
        Optional<Object> forecastObjForTimestamp = forecasts.stream().filter(f -> {
            String timestampString = (String) ((Map<String, Object>) f).get("dt_txt");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Timestamp timestamp = null;
            try {
                java.util.Date parsedDate = dateFormat.parse(timestampString);
                timestamp = new Timestamp(parsedDate.getTime());
            } catch (ParseException e) {
                logger.error("Unable to parse the date returned from weather API");
            }

            if (timestamp != null) {
                Timestamp threeEarly = new Timestamp(dateTime.getTime() - (1000 * 60 * 60 * 3));
                Timestamp threeLate = new Timestamp(dateTime.getTime() + (1000 * 60 * 60 * 3));

                return timestamp.after(threeEarly) && timestamp.before(threeLate);
            }
            return false;
        }).findFirst();

        if (forecastObjForTimestamp.isPresent()) {
            Map<String, Object> forecastObj = ((Map<String, Object>) forecastObjForTimestamp.get());
            String timestampString = (String)forecastObj.get("dt_txt");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Timestamp timestamp = null;
            try {
                java.util.Date parsedDate = dateFormat.parse(timestampString);
                timestamp = new Timestamp(parsedDate.getTime());
            } catch (ParseException e) {
                logger.error("Unable to parse the date returned from weather API");
            }
            forecast.setDateTime(timestamp);
            forecast.setTemperature((Double)((Map<String, Object>) forecastObj.get("main")).get("temp"));
            forecast.setHumidity((Double)((Map<String, Object>) forecastObj.get("main")).get("humidity"));
            Map<String, Object> weatherObj = (Map<String, Object>)(((List<Object>) forecastObj.get("weather")).stream().findFirst().get());
            forecast.setRain((String) weatherObj.get("description"));
            forecast.setCloud((Double)((Map<String, Object>) forecastObj.get("clouds")).get("all"));
            forecast.setWindSpeed((Double)((Map<String, Object>) forecastObj.get("wind")).get("speed"));

            forecastList.add(forecast);
        }


        return forecastList;
    }

    private Map<String, Object> callWeatherAPI(String url, String cityName) throws WeatherException {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("q", "{cityName}")
                .queryParam("appid", "{APIKey}")
                .encode().toUriString();

        Map<String, String> params = new HashMap<>();
        params.put("cityName", cityName);
        params.put("APIKey", API_KEY);

        HttpEntity<String> httpEntity = null;
        try {
            logger.info("Calling weather API: " + urlTemplate.toString());
            httpEntity = restTemplate.exchange(urlTemplate, HttpMethod.GET, entity, String.class, params);
        } catch (RestClientException e) {
            logger.error("Error when connecting to the weather API: " + e);
            throw new WeatherException("Error when connecting to the weather API: " + e);
        }

        String jsonString = httpEntity.getBody();
        logger.info("Weather API results:" + jsonString);

        return new Gson().fromJson(
                jsonString, new TypeToken<HashMap<String, Object>>() {
                }.getType()
        );
    }
}
