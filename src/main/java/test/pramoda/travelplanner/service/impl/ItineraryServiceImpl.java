package test.pramoda.travelplanner.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.pramoda.travelplanner.model.Forecast;
import test.pramoda.travelplanner.model.Itinerary;
import test.pramoda.travelplanner.repository.ForecastRepository;
import test.pramoda.travelplanner.repository.ItineraryRepository;
import test.pramoda.travelplanner.service.ItineraryService;

import java.util.HashSet;
import java.util.List;

@Service
public class ItineraryServiceImpl implements ItineraryService {

    @Autowired
    private ItineraryRepository itineraryRepository;

    @Autowired
    private ForecastRepository forecastRepository;

    @Override
    public Itinerary findItineraryByName(String name) {
        return itineraryRepository.findByName(name);
    }

    @Override
    @Transactional
    public void saveItinerary(String name, List<Forecast> forecasts) {
        Itinerary itinerary = itineraryRepository.findByName(name);
        if (itinerary == null) {
            itinerary = new Itinerary(name);
        }
        itinerary.setForecasts(new HashSet<>(forecasts));
        Itinerary finalItinerary = itinerary;
        forecasts.forEach(forecast -> forecast.getItineraries().add(finalItinerary));
        itineraryRepository.save(itinerary);
    }
}
