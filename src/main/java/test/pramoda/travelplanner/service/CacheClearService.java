package test.pramoda.travelplanner.service;

public interface CacheClearService {

    void clearRecords();
}
