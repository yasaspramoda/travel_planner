package test.pramoda.travelplanner.service;

import test.pramoda.travelplanner.model.Forecast;
import test.pramoda.travelplanner.model.Itinerary;

import java.util.List;

public interface ItineraryService {

    Itinerary findItineraryByName(String name);

    void saveItinerary(String name, List<Forecast> forecasts);
}
