package test.pramoda.travelplanner.service;

import test.pramoda.travelplanner.exception.WeatherException;
import test.pramoda.travelplanner.model.Forecast;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface WeatherService {

    String weatherURL = "http://api.openweathermap.org/data/2.5/weather";
    String forecastURL = "http://api.openweathermap.org/data/2.5/forecast";
    String API_KEY = "ad2291b8eb1ed42db6c8bada79d250dc";

    Map<String, Object> getWeatherForCity(String cityName) throws WeatherException;

    List<Forecast> getForecastForCity(String cityName, String dateTime) throws WeatherException;

}
