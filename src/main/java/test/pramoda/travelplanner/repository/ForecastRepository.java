package test.pramoda.travelplanner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import test.pramoda.travelplanner.model.Forecast;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ForecastRepository extends JpaRepository<Forecast, Long> {

    @Query(value = "SELECT * FROM forecast WHERE " +
            " city = ?1 AND " +
            " datetime >= ?2 - INTERVAL '3' HOUR AND " +
            " datetime <= ?2 + INTERVAL '3' HOUR ", nativeQuery = true)
    List<Forecast> findByCityAndDateTime(String city, Timestamp dateTime);

    @Query(value = "SELECT * FROM forecast WHERE " +
            " created_at < ?1 - INTERVAL '1' HOUR ", nativeQuery = true)
    List<Forecast> findByCreatedAtGreaterThanHour(Timestamp referenceTime);

}
