package test.pramoda.travelplanner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.pramoda.travelplanner.model.Itinerary;

import java.util.List;

public interface ItineraryRepository extends JpaRepository<Itinerary, Long> {

    Itinerary findByName(String name);

}
