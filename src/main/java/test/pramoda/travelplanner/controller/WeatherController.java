package test.pramoda.travelplanner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import test.pramoda.travelplanner.exception.WeatherException;
import test.pramoda.travelplanner.model.Forecast;
import test.pramoda.travelplanner.service.WeatherService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @GetMapping("/weather")
    public Map<String, Object> getWeather(@RequestParam String cityName) {
        try {
            Map<String, Object> weatherResult = weatherService.getWeatherForCity(cityName);
            return filterSelectedFields(weatherResult);
        } catch (WeatherException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred", e);
        }
    }

    // Please specify the Front end server URL below if you're running the React front end on the same server to enable CORS!!
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/forecast")
    public List<Forecast> getForecast(@RequestParam String cityName, @RequestParam String dateTime) {
        try {
            List<Forecast> forecastResult = weatherService.getForecastForCity(cityName, dateTime);
            return forecastResult;
        } catch (WeatherException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred", e);
        }
    }

    private Map<String, Object> filterSelectedFields(Map<String, Object> weatherResult) {
        Map<String, Object> selectedResults = new HashMap<>();
        selectedResults.put("City Name", weatherResult.get("name"));
        selectedResults.put("Country Code", ((Map<String, Object>)weatherResult.get("sys")).get("country"));
        selectedResults.put("Clouds", weatherResult.get("clouds"));

        return selectedResults;
    }
}
