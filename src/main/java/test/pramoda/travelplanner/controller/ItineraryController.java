package test.pramoda.travelplanner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.pramoda.travelplanner.model.Forecast;
import test.pramoda.travelplanner.model.Itinerary;
import test.pramoda.travelplanner.service.ItineraryService;

import java.util.List;
import java.util.Map;

@RestController
public class ItineraryController {

    @Autowired
    private ItineraryService itineraryService;

    // Please specify the Front end server URL below if you're running the React front end on the same server to enable CORS!!
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/itinerary")
    public Itinerary findItinerary(@RequestParam String name) {
        return itineraryService.findItineraryByName(name);
    }

    // Please specify the Front end server URL below if you're running the React front end on the same server to enable CORS!!
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/itinerary")
    public ResponseEntity<String> saveItinerary(@RequestBody Map<String, List<Forecast>> itinerary) {
        itinerary.keySet().forEach(name -> itineraryService.saveItinerary(name, itinerary.get(name)));
        return new ResponseEntity<>("Itinerary saved successfully", HttpStatus.OK);
    }

}
